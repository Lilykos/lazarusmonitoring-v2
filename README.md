Lazarus: The WiFi Monitoring Software (version 2)
===================

The final version of Lazarus Monitoring. It includes integration with web services so that it can be used from many PCs sending data to a deployed web service.
